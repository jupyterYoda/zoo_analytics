import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd

# referenece: https://gspread.readthedocs.io/en/latest/oauth2.html

def get_credentials():
    """builds api creds"""
    scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']
    credentials = ServiceAccountCredentials.from_json_keyfile_name(r'google_secrets_3.json', scope)
    return credentials

def get_survey_data(credentials):
    """gets raw data from the google sheet survey"""
    gc = gspread.authorize(credentials)

    wks = gc.open("Nashville Zoo Program Evaluations (Responses)").get_worksheet(0)
    data = wks.get_all_values()

    return data

def convert_data_2_df(data):
    """formats output data into a dataframe"""
    # Convert to a DataFrame and render.

    df = pd.DataFrame.from_records(data[1:], columns=data[0])
    return df

# copy allows us to easily get back original data

def drop_quiz_columns(df):
  """drop execess columns which are for feedback and score
  these indicators are created by the forum because we 
  set it to quiz mode
  
  parameters:
    df: pandas dataframe with survey data
    
  returns:
    df: modified dataframe with columns dropped"""
  
  bad_indicators = ['[Score]', '[Feedback]']
  columns_to_drop = [col for col in df.columns if any( ind in col for ind in bad_indicators)]
  df.drop(columns_to_drop, axis=1, inplace=True)
  
  return df

def drop_score_column(df):
  """drops specific columns"""
  
  drop_column_list = ['Score']
  df.drop(drop_column_list, axis=1, inplace=True)
  
  return df
  

def format_columns(df):
  """formats survey data how we need it for analysis"""
  
  # change columsn to numeric if possible
  return df.apply(pd.to_numeric, errors='coerce')

def set_column_datatypes(df):
  """formats the datatypes of the remaining columns
  expected all columns are returned as object type (string)
  so we need to convert to time or int"""
  
  DEFAULT = 'int64'
  meta_columns = ['Timestamp', 
                'What is the name of the program?', 
                'Is this a pre or post evaluation?', 
                'What is your audience for the program?',
                'What type of program are you evaluating?',
                'index']
  
  df['Timestamp'] = pd.to_datetime(df.Timestamp)
 
  for col in df.columns:
    if col not in meta_columns:
      df[col] = pd.to_numeric(df[col])
  
  return df

