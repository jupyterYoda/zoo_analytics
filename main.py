"""main processing script for the nashville zoo survey analysis
This takes in the raw data from the google sheets survey, 
formats the data and then exports back to the google sheets so the tableau can connect.
"""

from scripts import *

creds = get_credentials()
data = get_survey_data(creds)
data_mod = convert_data_2_df(data)

df = drop_quiz_columns(data_mod)
df = drop_score_column(df)
#df = set_column_datatypes(df)
#df = format_columns(df)

META_COLUMNS = ['Timestamp', 
                'What is the name of the program?', 
                'Is this a pre or post evaluation?', 
                'What is your audience for the program?',
                'What type of program are you evaluating?',
                'index']
df_columns = [col for col in df.columns if col not in META_COLUMNS]
output = []
for index, row in df.iterrows():
  entry = {}
  entry = {'record_id': index,
           'timestamp': row['Timestamp'],
           'program': row['What is the name of the program?'],
           'test_type': row['Is this a pre or post evaluation?'],
           'audience': row['What is your audience for the program?'],
           'program_type': row['What type of program are you evaluating?']
          }
  for i, value in row.iteritems():
    entry_mod = entry.copy()
    if (i in df_columns) & (len(value)>0):
      if value.isdigit():

        entry_mod['question'] =  i
        entry_mod['value'] = value
        output.append(entry_mod)
        
df_output = pd.DataFrame(output)


import gspread_dataframe as gd
gc = gspread.authorize(creds)

worksheet = gc.open('nashville_zoo_survey_data_modified').worksheet("data")

gd.set_with_dataframe(worksheet, df_output)
