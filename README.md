# Nashville Zoo Educational Survey Program
This describes the efforts performed to help the Nashville Zoo as part of the INFORMS Pro Bono Analytics project in 2018/2019.  The conatact for this material is Paul Jurek (pjurek3@gmail.com) or Chad Fifer (fifer@nashvillezoo.org).

## Overview
The survey data is collected using Google Forums and stored in Google Drive.  This data is pivoted for easier analysis in Tableau using a python script hosted on Python Anywhere which runs daily.  This outputs the data back to a new Google Drive sheet.  A [Tableau Dashboard](https://public.tableau.com/views/NashvilleZooSurveyAnalysis/SurveySummary?:embed=y&:display_count=yes&:origin=viz_share_link) on Tableau Public connects to this new data set and displays the results for analysis by the Zoo Team.  

## Survey Data
The survey data is collected through Google Forums.  This survey data is then stored in this [Google Sheet](https://docs.google.com/spreadsheets/d/1kAWlBKRcNsm8IeuUE8n3-at-mBN4vMd44Pca1TaB1_g/edit#gid=1243424911).  Each survey is a new record in the sheet. As of 2019.07.29, the data is stored in an expanding wide format where every survey has a column for every possible question.

## Data Processing
To support efficient use of Tableau, the data is pivoted from output of Google Forums.  This is accomplished through use of Python script on Python Anywhere.  The main functions of this script are to:
* pivot data
* reduce data size by removing blank columns
* ensure data consistency

## Deployment
The python script is deployed on the [Python Anywhere](https://www.pythonanywhere.com) website.  This script can be hosted elsewhere but needs to be scheduled.  In order to deploy it, a secrets file is needed to connect to google sheets.  Refer to Google documentation for details on this file format.  This process is scheduled to run once a day.  The output of this process is a new [google sheet](https://docs.google.com/spreadsheets/d/1zlVYoL77HPmTEiCnnXx1R81846rjdETuHwAtaZvJ5yE/edit#gid=0).  

The Tableau worksheet is hosted on Tableau Public (https://public.tableau.com/views/NashvilleZooSurveyAnalysis/SurveySummary?:embed=y&:display_count=yes&:origin=viz_share_link).  Anyone can download it and modify it as needed.  Downloading the free Tableau Public app is required.  

Contact is Paul Jurek (pjurek3@gmail.com) for this process.

